# Nextlabs Assignment

Live at https://nextlabs-assign.herokuapp.com/

## Local Setup

- Git clone: `git clone https://gitlab.com/14412gc/nextlabs-assignment.git`
- Go to the project directory: `cd nextlabs-assignment`
- Setup virtual environment: `virtualenv -p python3 .` (do not forget to add the dot(.) in the end)
- Activate virtual environment: `source bin/activate`
- Install requirements: `pip install -r requirements.txt`
- Run migrations: `python manage.py makemigrations`, `python manage.py migrate`
- Run server using local settings: `python manage.py runserver`
- See it running on [localhost](http://127.0.0.1:8000/)

## Tasks and Solutions

1. Basic python Write a regex to extract all the numbers in black colour text.

   Solution: [regular-expression/regular-exp.py](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/regular-expression/regular-exp.py)

2. Create a Django project, and an app - to store employee data. This model should have following properties : [firstName, lastName, employeeId, city]

   Solution: [myapp/models.py](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/myapp/models.py)

3. Get the data from the sheet - and post it to the newly created model as it’s object.

   Solution: [myapp/management/commands/upload_data.py](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/myapp/management/commands/upload_data.py)

   Note: To run this locally use the command `python manage.py upload_data` but before that you have to download your `credentials.json` as explained in [Google Sheets API](https://developers.google.com/sheets/api/quickstart/python#step_1_turn_on_the).
   After downloading that file move it to `nextlabs-assignment` directory.

4. Login - Registration Page
   Solution:

   - Register View: [myapp/views.py#L24](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/myapp/views.py#L24)
   - Login View: [myapp/views.py#L44](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/myapp/views.py#L44)
   - Templates: [myapp/templates/myapp](https://gitlab.com/14412gc/nextlabs-assignment/-/tree/master/myapp/templates/myapp)

5. REST API for Employee Model
   - ListCreateView: [myapp/views.py#L14](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/myapp/views.py#L14)
   - EmployeeSerializer: [myapp/serializers.py](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/myapp/serializers.py)
   - EndPoint: http://127.0.0.1:8000/api/employee

## Sample Video of Task#3

[Sample Video](https://gitlab.com/14412gc/nextlabs-assignment/-/blob/master/sample.mov)
