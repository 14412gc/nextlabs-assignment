from . import views
from django.urls import path
from django.views.generic import TemplateView

app_name = 'myapp'
urlpatterns = [
    path('', TemplateView.as_view(template_name='myapp/home.html'), name='home'),
    path('login', views.login_view, name='login'),
    path('register', views.register_view, name='register'),
    path('api/employee', views.EmployeeListCreateView.as_view(), name='employee'),
]
