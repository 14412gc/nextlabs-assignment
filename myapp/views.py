from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from django.urls import reverse
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate

from .models import Employee
from .serializers import EmployeeSerializer
from .forms import UserLoginForm, UserRegisterForm


class EmployeeListCreateView(generics.ListCreateAPIView):
    """
    EmployeeCreateView
    """
    model = Employee
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
    permission_classes = (IsAuthenticated,)


def register_view(request):
    """
    User Register View
    """
    form = UserRegisterForm()
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            User.objects.create_user(username=username, password=password)
            user_auth = authenticate(username=username, password=password)
            login(request, user_auth)
            return redirect(reverse('myapp:employee'))
    context = {
        'form': form
    }
    return render(request, "myapp/register.html", context)


def login_view(request):
    """
    User Login View
    """
    form = UserLoginForm()
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user_auth = authenticate(username=username, password=password)
            login(request,user_auth)
            return redirect(reverse('myapp:employee')) 
    context = {
        'form':form
    }
    return render(request, "myapp/login.html", context)
