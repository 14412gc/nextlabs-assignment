from django.db import models


class Employee(models.Model):
    firstName = models.CharField(max_length=30)
    lastName = models.CharField(max_length=30)
    employeeId = models.IntegerField()
    city = models.CharField(max_length=30)

    def __str__(self):
        return str(self.firstName)
