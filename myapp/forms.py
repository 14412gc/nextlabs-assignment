from django import forms
from django.contrib.auth.models import User 


class UserRegisterForm(forms.ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)
    
    class Meta:
        model = User
        fields = ('username', 'password',)
    
    def clean_username(self):
        """
        Validate Username field
        """
        username = self.cleaned_data.get('username')
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError('Username already taken')
        return username 


class UserLoginForm(forms.Form):
	username = forms.CharField(max_length=30)
	password = forms.CharField(widget=forms.PasswordInput)

	def clean(self, *args, **kwargs):
		username = self.cleaned_data.get('username') 
		password = self.cleaned_data.get('password')

		if username and password:
			user = User.objects.get(username=username)
			if not user:
				raise forms.ValidationError("user does not exsist")
			if not user.check_password(password):
				raise forms.ValidationError("Incorrect Password")
		return super(UserLoginForm, self).clean(*args, **kwargs)
